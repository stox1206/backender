package com.glovoapp.backender;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
class ApplicationConfiguration {
    private int maxForBicycle;
    private int slotDistance;
    private List<String> boxRequired;
    private Priority priority;
    
    public enum Priority {
        VIP, FOOD, NONE
    }
    
    public int getMaxForBicycle() {
        return maxForBicycle;
    }
    
    public void setMaxForBicycle(int maxForBicycle) {
        this.maxForBicycle = maxForBicycle;
    }
    
    public int getSlotDistance() {
        return slotDistance;
    }
    
    public void setSlotDistance(int slotDistance) {
        this.slotDistance = slotDistance;
    }
    
    public List<String> getBoxRequired() {
        return boxRequired;
    }
    
    public void setBoxRequired(List<String> boxRequired) {
        this.boxRequired = boxRequired;
    }
    
    public Priority getPriority() {
        return priority;
    }
    
    public void setPriority(Priority priority) {
        this.priority = priority;
    } 

}
