package com.glovoapp.backender;

import com.glovoapp.backender.ApplicationConfiguration.Priority;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Component
class OrderRepository {
    private static final String ORDERS_FILE = "/orders.json";
    private static final List<Order> orders;

    @Autowired
    private ApplicationConfiguration appConfig;
    
    static {
        try (Reader reader = new InputStreamReader(OrderRepository.class.getResourceAsStream(ORDERS_FILE))) {
            Type type = new TypeToken<List<Order>>() {
            }.getType();
            orders = new Gson().fromJson(reader, type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public OrderRepository(ApplicationConfiguration appConfig) {
        this.appConfig = appConfig;
    }

    List<Order> findAll() {
        return new ArrayList<>(orders);
    }

    List<Order> findOrderByCourier(Courier courier) {
        if (courier == null) return new ArrayList<Order>();
        
        Predicate<Order> isBoxNeed;
        if (!courier.getBox()) {
            isBoxNeed = o -> appConfig.getBoxRequired().stream()
                    .noneMatch(s -> o.getDescription().toLowerCase().contains(s));
        } else {
            isBoxNeed = o -> true;
        }

        Predicate<Order> isDistanceLimited;
        if (courier.getVehicle().equals(Vehicle.BICYCLE)) {
            isDistanceLimited = o -> getDistance(o, courier) < appConfig.getMaxForBicycle();
        } else {
            isDistanceLimited = o -> true;
        }
        
        List<Order> sortedOrders = orders.stream()
                .filter(isBoxNeed)
                .filter(isDistanceLimited)
                .sorted((o1, o2) -> Double.compare(getDistance(o1, courier), getDistance(o2, courier)))
                .collect(Collectors.toList());
                
        Collector<Order, ?, Map<Object, Map<Object, List<Order>>>> groupByVipAndFood;
        if (appConfig.getPriority().equals(Priority.VIP)) {
            groupByVipAndFood = Collectors.groupingBy(Order::getVip, Collectors.groupingBy(Order::getFood));
        } else if (appConfig.getPriority().equals(Priority.FOOD)) {
            groupByVipAndFood = Collectors.groupingBy(Order::getFood, Collectors.groupingBy(Order::getVip));
        } else {
        	return sortedOrders;
        }
        
        Map<Object, Map<Object, Map<Object, List<Order>>>> slots = sortedOrders.stream()
                .collect(Collectors.groupingBy(o -> Math.ceil((int) (getDistance(o, courier) * 1000) / appConfig.getSlotDistance()),
                        groupByVipAndFood));

        return slots.entrySet().stream()
        .sorted((e1, e2) -> Double.compare((double) e1.getKey(), (double) e2.getKey()))
        .flatMap(l1 -> l1.getValue().entrySet().stream()
                .sorted((e1, e2) -> (-1 * Boolean.compare((boolean) e1.getKey(), (boolean) e2.getKey())))
                .flatMap(l2 -> l2.getValue().entrySet().stream()
                        .sorted((e1, e2) -> (-1 * Boolean.compare((boolean) e1.getKey(), (boolean) e2.getKey())))
                        .flatMap(l3 -> l3.getValue().stream())))
        .collect(Collectors.toList());
    }

    private double getDistance(Order order, Courier courier) {
        return DistanceCalculator.calculateDistance(courier.getLocation(), order.getPickup());
    }
}

