package com.glovoapp.backender;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.glovoapp.backender.ApplicationConfiguration.Priority;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@EnableAutoConfiguration
@SpringJUnitConfig(ApplicationConfiguration.class)
class OrderRepositoryTest {

    @Autowired
    private ApplicationConfiguration appConfig;
    
    @Test
    void findAll() {
        List<Order> orders = new OrderRepository(appConfig).findAll();

        assertFalse(orders.isEmpty());

        Order firstOrder = orders.get(0);

        Order expected = new Order().withId("order-1")
                .withDescription("I want a pizza cut into very small slices")
                .withFood(true)
                .withVip(false)
                .withPickup(new Location(41.3965463, 2.1963997))
                .withDelivery(new Location(41.407834, 2.1675979));

        assertEquals(expected, firstOrder);
    }
    
    @Test
    void findOrderByInvalidCourier() {
        Courier courier = new CourierRepository().findById("bad-courier-id");
        List<Order> orders = new OrderRepository(appConfig).findOrderByCourier(courier);
        assertTrue(orders.isEmpty());
    }
    
    @Test
    void findOrderByCourierWithBox() {
        Courier courier = new CourierRepository().findById("courier-1");
        List<Order> orders = new OrderRepository(appConfig).findOrderByCourier(courier);
        
        Order filtered = orders.stream()
                .filter(o -> appConfig.getBoxRequired().stream()
                        .anyMatch(s -> o.getDescription().toLowerCase().contains(s)))
                .findFirst()
                .orElse(null);
        
        assertNotNull(filtered);
    }
    
    @Test
    void findOrderByCourierWithNoBox() {
        Courier courier = new CourierRepository().findById("courier-1-nobox");
        List<Order> orders = new OrderRepository(appConfig).findOrderByCourier(courier);

        Order filtered = orders.stream()
                .filter(o -> appConfig.getBoxRequired().stream()
                        .anyMatch(s -> o.getDescription().toLowerCase().contains(s)))
                .findFirst()
                .orElse(null);
        
        assertNull(filtered);
    }

    @Test
    void findOrderByCourierByScooter() {
        Courier courier = new CourierRepository().findById("courier-2");
        List<Order> orders = new OrderRepository(appConfig).findOrderByCourier(courier);

        Order filtered = orders.stream()
                .filter(o -> o.getId().equals("order-13"))
                .findFirst()
                .orElse(null);
        
        assertNotNull(filtered);
        assertTrue(getDistance(filtered, courier) > appConfig.getMaxForBicycle());
    }
    
    @Test
    void findOrderByCourierByBicycle() {
        Courier courier = new CourierRepository().findById("courier-3");
        List<Order> orders = new OrderRepository(appConfig).findOrderByCourier(courier);

        Order filtered = orders.stream()
                .filter(o -> o.getId().equals("order-13"))
                .findFirst()
                .orElse(null);
        
        assertNull(filtered);
    }
    
    @Test
    void prioritizeVip() {
        Courier courier = new CourierRepository().findById("courier-2");
        List<Order> orders = new OrderRepository(appConfig).findOrderByCourier(courier);

        assertTrue(orders.get(0).getId().equals("order-3-flamingo-vip"));
        assertTrue(orders.get(6).getId().equals("order-2-vip"));
        assertTrue(orders.get(8).getId().equals("order-2"));
    }
    
    @Test
    void prioritizeFood() {
        appConfig.setPriority(Priority.FOOD);
        Courier courier = new CourierRepository().findById("courier-2");
        List<Order> orders = new OrderRepository(appConfig).findOrderByCourier(courier);

        assertTrue(orders.get(2).getId().equals("order-3-flamingo-vip"));
        assertTrue(orders.get(6).getId().equals("order-2-vip"));
        assertTrue(orders.get(7).getId().equals("order-2"));
        appConfig.setPriority(Priority.VIP);
    }
    
    @Test
    void prioritizeNone() {
        appConfig.setPriority(Priority.NONE);
        Courier courier = new CourierRepository().findById("courier-2");
        List<Order> orders = new OrderRepository(appConfig).findOrderByCourier(courier);

        assertTrue(orders.get(0).getId().equals("order-10"));
        assertNotNull(orders);
        appConfig.setPriority(Priority.VIP);
    }
    
    @Test
    void changeSlotDistance() {
        appConfig.setSlotDistance(1000);
        Courier courier = new CourierRepository().findById("courier-2");
        List<Order> orders = new OrderRepository(appConfig).findOrderByCourier(courier);

        assertTrue(orders.get(1).getId().equals("order-3-flamingo-vip"));
        assertTrue(orders.get(0).getId().equals("order-2-vip"));
        assertTrue(orders.get(6).getId().equals("order-2"));
        assertTrue(orders.get(16).getId().equals("order-1-vip"));
        appConfig.setSlotDistance(500);
    }

    private double getDistance(Order order, Courier courier) {
        return DistanceCalculator.calculateDistance(courier.getLocation(), order.getPickup());
    }
}